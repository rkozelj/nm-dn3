# naj asistent ignorira spodnji dve vrstici
#cd("D:\\git\\Num-mat\\DN\\dn3\\nm-dn3\\doc\\src\\domace\\03\\rkozelj")
#include("plots.jl")


using Plots
using SpecialFunctions


include("..\\..\\..\\..\\..\\src\\domaca03\\rkozelj\\airijeva_funkc.jl")

"""
    plot_airy(x0, y0, h, num_zeros)

Izriše airijevo funkcijo v točkah od x0 do ničle `num_zeros`.
"""
function plot_airy(x0, y0, h, num_zeros)
    start_time = time()

    X, Y, N = magnus_method_until_enough_zeros(x0, y0, h, num_zeros)
    println("time: ", time() - start_time, " step: ", h)

    plot(X, Y[1], color = "blue", legend=false, xlabel = "x", ylabel = "izracunane  vrednosti  Airijeve  funkcije", title = string("h = ", h))
    savefig("images\\airy.png")
end


"""
    plot_airy_error(x0, y0, h, num_zeros)

Izriše razliko vrednosti airijeve funkcije dobljenih z magnusovo metodo s pravimi vrednostmi dobljenimi s funkcijo airyai na intervalu od x0 do ničle `num_zeros`.
"""
function plot_airy_error(x0, y0, h, num_zeros)
    start_time = time()
    X, Y, N = magnus_method_until_enough_zeros(x0, y0, h, num_zeros)
    println("time: ", time() - start_time, " step: ", h)

    errors = [Y[1][i] - airyai(X[i]) for i = 1:length(X)]
    plot(X, errors, color = "red", legend=false, xlabel = "x", ylabel = "napaka  izracunanih  vrednosti  Airijeve  funkcije", title = string("h = ", h))
    savefig("images\\error.png")

    #abs_errors = [abs(Y[1][i] - airyai(X[i])) for i = 1:length(X)]
    #plot(X, abs_errors, color = "red", legend=false, xlabel = "x", ylabel = "absolutna  napaka  izracunanih  vrednosti  Airijeve  funkcije", title = string("h = ", h))
    #savefig("images\\error_abs.png")
end


"""
    plot_airy_zeros_error(x0, y0, h, num_zeros, h_forced)

Izriše napako najdenih prvih `num_zeros` ničel airijeve funkcije.
"""
function plot_airy_zeros_error(x0, y0, h, num_zeros, h_forced)
    start_time = time()
    x_zeros, y_zeros, h = find_n_zeros(x0, y0, num_zeros; method = "bisection", h_forced = h_forced)
    #println(x_zeros)
    #println(y_zeros)
    println("time: ", time() - start_time, " bisection")

    zeros_idxs = [i for i = 1:num_zeros]
    #errors = [y_zeros[i] - airyai(x_zeros[i]) for i in 1:num_zeros]
    errors_bisection = [y_zeros[i] - 0 for i in 1:num_zeros]
    scatter(zeros_idxs, errors_bisection, color = "red", legend=false, xlabel = "stevilka  nicle", ylabel = "napaka  izracunanih  nicel", title = string("h = ", h))
    savefig("images\\zeros_error_bisection.png")

    
    start_time = time()
    x_zeros, y_zeros = find_n_zeros(x0, y0, num_zeros; method = "newton", h_forced = h_forced)
    #println(x_zeros)
    #println(y_zeros)
    println("time: ", time() - start_time, " newton")

    zeros_idxs = [i for i = 1:num_zeros]
    #errors = [y_zeros[i] - airyai(x_zeros[i]) for i in 1:num_zeros]
    errors_newton = [y_zeros[i] - 0 for i in 1:num_zeros]
    plot(zeros_idxs, errors_newton, color = "red", legend=false, xlabel = "stevilka  nicle", ylabel = "napaka  izracunanih  nicel", title = string("h = ", h))
    savefig("images\\zeros_error_newton.png")


    plot(zeros_idxs, errors_bisection, color = "blue", xlabel = "stevilka  nicle", ylabel = "napaka  izracunanih  nicel", label = "bisection", title = string("h = ", h))
    plot!(zeros_idxs, errors_newton, color = "red", label = "newton")
    savefig("images\\zeros_error.png")

    #abs_errors = [abs(y_zeros[i] - 0) for i in 1:num_zeros]
    #plot(zeros_idxs, abs_errors, color = "red", legend=false, xlabel = "stevilka  nicle", ylabel = "abolutna  napaka  izracunanih  nicel")
    #savefig("images\\zeros_error_abs.png")
    
end

x0 = 0
y0 = [1/(3^(2/3) * gamma(2/3)), -1/(3^(1/3) * gamma(1/3))]

num_zeros = 288

h0 = -1/10
h, N = find_h(x0, y0, h0, num_zeros)

#plot_airy(x0, y0, h, num_zeros)
#plot_airy_error(x0, y0, h, num_zeros)
#plot_airy_zeros_error(x0, y0, h, num_zeros, 0.0)

