﻿# Ničle Airyjeve funkcije

V nalogi računam ničle Airijeve funkcije $`Ai`$. Airijeva funkcija je dana kot rešitev začetnega problema
```math
  Ai''(x)-x\,Ai(x)=0
```
```math
    Ai(0)=\frac{1}{3^\frac{2}{3}\Gamma\left(\frac{2}{3}\right)},
    Ai'(0)=-\frac{1}{3^\frac{1}{3}\Gamma\left(\frac{1}{3}\right)}.
    
```

Za računanje $`y(x)`$ sem uporabila Magnusovo metodo reda 4 za reševanje enačb oblike
```math
   y'(x) = Ai(x)
```
pri kateri nov približek $`\mathbf{Y}_{k+1}`$ dobim s spodnjim postopkom
```math
    A_1=A\Big(x_k+\Big(\frac{1}{2}-\frac{\sqrt{3}}{6}\Big)h\Big)
```
```math
    A_2=A\Big(x_k+\Big(\frac{1}{2}+\frac{\sqrt{3}}{6}\Big)h\Big)
```
```math
    \sigma_{k+1} = \frac{h}{2}(A_1+A_2)-\frac{\sqrt{3}}{12}h^2[A_1,A_2]
```
```math
    \mathbf{Y}_{k+1}=\exp(\sigma_{k+1})\mathbf{Y}_k
```
Matrika $`A(x)`$ je v primeru te naloge spodnje oblike (vir: [Magnus method](http://www.damtp.cam.ac.uk/user/na/people/Marianna/papers/MagnusFilon.pdf) strani 9, 10, 11)
```math
A(x)=\begin{bmatrix}
0 & 1\\
x & 0
\end{bmatrix}.
```
 
Za dovolj natančen izračun funkcijskih vrednosti sem morala najprej poiskati ustrezen korak. Ustrezen korak poišče funkcija 
`find_h(x0, y0, h0, num_zeros; epsilon = 10^(-10), method = "last value")`, ki na intervalu od $`x_0`$ do $`x`$, kjer leži zadnja iskana ničla,
izračuna funkcijske vrednosti najprej s korakom $`h = h_0`$ ter nato s polovico manjšim korakom. Korak manjša za faktor dva, dokler se zadnji izračunan $`y`$
(tisti pri nazadnje najdeni ničli) izračunan s korakom $`h`$ in tisti izračunan s korakom $`h/2`$ po absolutni vrednosti razlikujeta več kot za `epsilon`. 
Ta metoda se mi zdi ok, ker je lokalna napaka $`y`$ v koraku $`i`$ odvisna še od vseh prejšnjih napak
in zato sklepam, da v primeru, ko je zadnji izračunan $`y`$, na določenem intervalu, izračunan dovolj natančno, so verjetno tudi vsi prejšnji. Se pa bojim, da lahko,
zaradi napak v negativni in pozitivni smeri, to ne drži v vseh primerih. V primeru te naloge, sem lahko z opisano metodo poiskala dovolj natančne vrednosti funkcije do 
vključno 288. ničle.

Spodnji levi graf prikazuje izris Airijeve funkcije do 288. ničle, kjer so vrednosti dobljene z omenjeno Magnusovo metodo, korak $`h`$ pa po zgornji metodi.
Spodnji desni graf prikazuje napako teh izračunanih vrednosti s pravimi vrednostmi dobljenimi s fukcijo `airyai` iz paketa `SpecialFunctions.jl`.

<img src="images/airy_288_00625.png" alt="airy" width="550" height="370"/>
<img src="images/error_288_00625.png" alt="airy error" width="550" height="370"/>

Implementirala sem še dve drugi metodi za izračun koraka, ki pa se nista dobro odnesli (razloženo v kodi).

Preden sem se lotila iskanja pravih vrednosti ničel, sem najprej poiskala dobre začene približke zanje z uporabo funkcije `find_aprox_n_zeros(x0, y0, h, num_zeros)`.
Funkcija s podanim `h` računa funkcijske vrednosti na intervalu od $`x_0`$ do $`x`$, kjer leži zadnja iskana ničla, ter na vsakem koraku
preveri predznaka funkcijskih vrednosti. Če se predznaka razlikujeta, se vmes nahaja ničla. Tako sem si za vsako ničlo zabaležila $`x`$ koordinati, ki
ničlo zaobjemata.

S temi približki sem za vsako od ničel z uporabo bisekcije in Newtonove metode poiskala dovolj natančno vrednost ničel. Da sem našla vrednosti, ki so na
željeno natančnost blizu ničelne, prikazujem s spodnjim grafom.

<img src="images/zeros_error_288_00625.png" alt="airy zeros error" width="550" height="370"/>



<!-- JA ITAK SEJ VALDA JE NIČLA VMES TUD ČE FUN VREDNOSTI NISO DOBR ZRAČUNANE, SAM TA NIČLA NI NA TA PRAVEM MESTU
Ker sem se kar nekaj čas igrala z implementacijo metode za iskanje dovolj natančnega koraka, sem med tem poizkusila poiskati ničle tudi z večjimi koraki
in ob tem ugotovila, da čeprav vrednsoti Airijeve funkcije z manjšimi `h` kot je - 0.0625 ne dajo zadovolje natančnosti, so ničle kjub temu izračunane dvolj natančno.
Rezultate s korakom -1/5 prikazujem na spodnjih treh grafih.

<img src="images/airy_100_02.png" alt="airy" width="550" height="370"/>
<img src="images/error_100_2.png" alt="airy error" width="550" height="370"/>
<img src="images/zeros_error_100_02.png" alt="airy error" width="550" height="370"/>

Za korak bo abs manjši od - 1/5 newtonova metoda lahko računa neskočno časa.-->
