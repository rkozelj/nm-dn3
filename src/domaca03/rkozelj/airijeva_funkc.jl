# naj asistent ignorira spodnji dve vrstici
#cd("D:\\git\\Num-mat\\DN\\dn3\\nm-dn3\\src\\domaca03\\rkozelj")
#include("airijeva_funkc.jl")


using SpecialFunctions

"""
    get_x_y_h_adaptive(X, Y, h)

Funkcija izračuna naslednji x, y (vrednost airijeve funkcije) in dy, ki se nahajajo na zadnjih mestih v seznamih
    X = [xes], 
    Y = [list_of_yons, list_of_derivatives]
ter h s katerim y izračuna na natančnost 10^(-10).

Funkcija je spisana kot adaptivna metoda, ki glede na razliko med vrednostjo airijeve funkcije izračunane s h in tiste s h/2,
zmanša h za polovico, ga poveča za polovico ali ga obdrži.

FUNKCIJA NI UPORABLJENA, SAJ SEM PO IZRISU GRAFA RAZLIK MED AIRIJEVO FUNKCIJO IZRAČUNANO S TEMI ADAPTIVNIMI KORAKI IN PRAVIMI VREDNOSTMI
UGOTOVILA, DA SO RAZLIKE REDA 10^(-1) (NAMESTO 10^(-10)). ZAKAJ JE TAKO, NE VEM.
"""
function get_x_y_h_adaptive(X, Y, h)
    #izračun za korak h od x0
    X_at_h, Y_at_h = magnus_method(X[end], [Y[1][end], Y[2][end]], 1, h) # en izračun za korak h
    x_at_h = X_at_h[end]
    y_at_h = Y_at_h[:, end]

    X_more_precise_at_h, Y_more_precise_at_h = magnus_method(X[end], [Y[1][end], Y[2][end]], 2, h/2) # dva izračuna s korakom h/2
    x_more_precise_at_h = X_more_precise_at_h[end]
    y_more_precise_at_h= Y_more_precise_at_h[:, end]

    #izračun za korak 2h od x0
    X_at_2h, Y_at_2h = magnus_method(X[end], [Y[1][end], Y[2][end]], 2, h) # dva izračuna s korakom h
    x_at_2h = X_at_2h[end]
    y_at_2h= Y_at_2h[:, end]
        
    X_less_precise_at_2h, Y_less_precise_at_2h = magnus_method(X[end], [Y[1][end], Y[2][end]], 1, 2*h) # en izračun s korakom 2h
    x_less_precise_at_2h = X_less_precise_at_2h[end]
    y_less_precise_at_2h = Y_less_precise_at_2h[:, end]

    """
    println("x: ", x_at_h, " ", x_more_precise_at_h)
    println("abs y more precise: ", abs(y_at_h[1] - y_more_precise_at_h[1]))
    println("abs y less precise: ", abs(y_at_2h[1] - y_less_precise_at_2h[1]))
    println()
    sleep(5)
    """

    if abs(y_at_h[1] - y_more_precise_at_h[1]) >= 10^(-10) # če je razlika med y pri h in tistim pri 2*h/2 prevelika, korak še manjšamo
        while abs(y_at_h[1] - y_more_precise_at_h[1]) >= 10^(-10)
            
            h = h/2
            
            X_at_h, Y_at_h = magnus_method(X[end], [Y[1][end], Y[2][end]], 1, h)
            x_at_h = X_at_h[end]
            y_at_h = Y_at_h[:, end]

            X_more_precise_at_h, Y_more_precise_at_h = magnus_method(X[end], [Y[1][end], Y[2][end]], 2, h/2)
            x_more_precise_at_h = X_more_precise_at_h[end]
            y_more_precise_at_h= Y_more_precise_at_h[:, end]

            """
            println("h more precise: ", h)
            println("x: ", x_at_h, " ", x_more_precise_at_h)
            println("abs y: ", abs(y_at_h[1] - y_more_precise_at_h[1]))
            sleep(5)
            """
        end

        return X_more_precise_at_h, Y_more_precise_at_h, h

    elseif abs(y_at_2h[1] - y_less_precise_at_2h[1]) <= 10^(-11) # če je razlika med y pri 2h in tistim pri 2*h premajhna, korak povečamo
        
        while abs(y_at_2h[1] - y_less_precise_at_2h[1]) <= 10^(-11)
            h = 2*h
            X_at_2h, Y_at_2h = magnus_method(X[end], [Y[1][end], Y[2][end]], 2, h)
            x_at_2h = X_at_2h[end]
            y_at_2h= Y_at_2h[:, end]
                
            X_less_precise_at_2h, Y_less_precise_at_2h = magnus_method(X[end], [Y[1][end], Y[2][end]], 1, h)
            x_less_precise_at_2h = X_less_precise_at_2h[end]
            y_less_precise_at_2h = Y_less_precise_at_2h[:, end]
            """
            println("h less precise: ", h)
            println("x: ", x_at_2h, " ", x_less_precise_at_2h)
            println("abs y: ", abs(y_at_2h[1] - y_less_precise_at_2h[1]))
            sleep(5)
            """

            if abs(y_at_2h[1] - y_less_precise_at_2h[1]) <= 10^(-10) # če se vrednosti začnejo preveč razlikovati, je treba vrnit prejšnji izračun
                """
                println("vračamo prejšnji izračun")
                println("x: ", x_at_2h[end-1])
                println("y: ", Y_at_2h[:, end-1][1])
                sleep(5)
                """
                return X_at_2h[1:end-1], Y_at_2h[:, 1:end-1], h/2
            end
        end

        return X_at_2h, Y_less_precise_at_2h, h
    else
        """
        println("all good")
        sleep(5)
        """
        return X_at_h, Y_at_h, h
    end
end

"""
    magnus_method(x0, y0, N, h)

Z uporabo magnusove metode reda 4 izračuna vrednosti in odvode arijeve funkcije na območju od x0 do x0 + h*N s korakom `h`.
`y0` je seznam, katerega prvi element predstavlja vrednost Airijeve funkcije v `x0`, drugi pa predstavlja vrednost odvoda v `x0`.

Vrne 
    seznam X, ki vsebuje vrednosti x-ov na katerih je izračunala vrednost funkcije, 
    seznam Y, ki v prvi vrstici vsebuje vrednosti y, v drugi pa vrednosti dy v točkah x iz seznama X
"""
function magnus_method(x0, y0, N, h)
    function A(x)
         return [0 1; x 0]
    end

    x = x0
    y = y0
    Y = zeros(length(y0), N + 1)
    X = zeros(1, N + 1)
    Y[:, 1] = y
    X[1] = x
    
    for i = 2:N+1
        A1 = A(x+(0.5-sqrt(3)/6)*h)
        A2 = A(x+(0.5+sqrt(3)/6)*h)
        sigma = (h/2)*(A1+A2)-(sqrt(3)/12)*(h^2)*(A1*A2-A2*A1)
        y = exp(sigma)*[y[1];y[2]]
        x = x + h
        
        Y[:, i] = y
        X[i] = x
    end
        
    return X, Y
end

"""
    function magnus_method_until_enough_zeros(x0, y0, h, num_zeros)

Z uporabo magnusove metode reda 4 izračuna vrednosti in odvode arijeve funkcije na območju od x0 do `num_zeros` ničle. 
Računa s korakom `h`. `y0` je seznam, katerega prvi element predstavlja vrednost Airijeve funkcije v `x0`, drugi pa predstavlja vrednost odvoda v `x0`.

Vrne 
    seznam X, ki vsebuje vrednosti x-ov na katerih je izračunala vrednost funkcije, 
    seznam Y, ki v prvi vrstici vsebuje vrednosti y, v drugi pa vrednosti dy v točkah x iz seznama X
    število intervalov na katere je bilo potrebno razdeliti območje med x0 in x na katerem leži najbolj oddaljena ničla
"""
function magnus_method_until_enough_zeros(x0, y0, h, num_zeros)
    x = x0
    y = y0
    X = []
    Y = [[y0[1]], [y0[2]]]
    push!(X, x)
     
    num_found_zeros = 0
    N = 0

    while num_found_zeros < num_zeros
        """
        println("h current: ", h)
        sleep(2)
        """
        X_current, Y_current = magnus_method(X[end], [Y[1][end], Y[2][end]], 1, h)
        x = X_current[end]
        y = Y_current[:, end]

        #X_current, Y_current, h = get_x_y_h_adaptive(X, Y, h) # adaptivno spreminjanje koraka - ni šlo skozi
        #x = X_current[end]
        #y = Y_current[:, end]
        #println("h used: ", h)
        #println(x, " ", y[1])
        #sleep(2)

        push!(Y[1],y[1])
        push!(Y[2],y[2])
        push!(X, x)

        if Y[1][end] * Y[1][end-1] < 0
            num_found_zeros += 1
        end
        N += 1
    end
            
    return X, Y, N
end

"""
    find_aprox_n_zeros(x0, y0, h, num_zeros)

Najde dobre približke za prvih `num_zeros` ničel od x0 naprej, kjer vrednosti funkcije računa s korakom `h`.
`y0` je seznam, katerega prvi element predstavlja vrednost Airijeve funkcije v `x0`, drugi pa predstavlja vrednost odvoda v `x0`.

Približke vrne v obliki seznamov 
    X_zeros_aprox = [[x1_aprox_zero_1, x2_aprox_zero_1], [x1_aprox_zero_2, x2_aprox_zero_2], ..., [x1_aprox_zero_num_zeros, x2_aprox_zero_num_zeros]
    Y_zeros_aprox = [[[y1_aprox_zero_1, dy1_aprox_zero_1], [y2_aprox_zero_1, dy2_aprox_zero_1]], [[y1_aprox_zero_2, dy1_aprox_zero_2], [y2_aprox_zero_2, dy2_aprox_zero_2]], 
..., [[y1_aprox_zero_num_zeros, dy1_aprox_zero_num_zeros], [y2_aprox_zero_num_zeros, dy2_aprox_zero_num_zeros]]]
"""
function find_aprox_n_zeros(x0, y0, h, num_zeros)  
    x = x0
    y = y0
    X = []
    push!(X, x)
    Y = [[y0[1]], [y0[2]]]
    
    X_zeros_aprox = [[0.0, 0.0] for i=1:num_zeros]
    Y_zeros_aprox = [[[0.0, 0.0], [0.0, 0.0]] for i=1:num_zeros]

    N = 0
    num_found_zeros = 0
    while num_found_zeros < num_zeros
        #println("h current: ", h)
        #sleep(2)
        
        X_current, Y_current = magnus_method(X[end], [Y[1][end], Y[2][end]], 1, h)
        x = X_current[end]
        y = Y_current[:, end]
        
        push!(Y[1],y[1])
        push!(Y[2],y[2])
        push!(X, x)
       
        if Y[1][end] * Y[1][end-1] < 0 # če se prejšnji in trenutni y razlikujeta po predznaku, potem je vmes ničla # shranim obe vrednosti za bisekcijo
            num_found_zeros += 1
            X_zeros_aprox[num_found_zeros] = [X[end], X[end-1]]
            Y_zeros_aprox[num_found_zeros] = [[Y[1][end], Y[2][end]], [Y[1][end-1], Y[2][end-1]]] 
        end
        N += 1

    end
            
    return X_zeros_aprox, Y_zeros_aprox
end

"""
    find_h(x0, y0, h0, num_zeros; epsilon = 10^(-10), method = "last value")

Najde korak `h` s katerim bodo vrednosti Airijeve funkcije do `num_zeros` ničle izračunane na natančnost `epsilon`.
Začne z h = `h0` in korak razpolavlja dokler
    - v primeru ko method = "last value"
    se vrednosti y (ki je vrednost Airijeve funkcije v okolici zadnje najdene ničle (ničla, ki leži najbolj stran od x0)) 
    izračunani s korakoma h in h/2 razlikujeta za več kot `epsilon`.
    - v primeru ko method = "all values" - V TEM PRIMERU KORAK RAZPOLAVLJA V NEDOGLED, (ko sem poizkusila z zaokroževanjem na 2 decimalni mesti (in ne na 10) je pa vse ok)
    se katerakoli od izračnanih vrednosti z večjim korakom razlikuje od tiste z manjšim za več kot `epsilon`.

`y0` je seznam, katerega prvi element predstavlja vrednost Airijeve funkcije v `x0`, drugi pa predstavlja vrednost odvoda v `x0`.
Funkcija vrne h in N - število intervalov na katere je bilo potrebno razdeliti območje med x0 in x na katerem leži najbolj oddaljena ničla.
"""
function find_h(x0, y0, h0, num_zeros; epsilon = 10^(-10), method = "last value") #method mora biti "last value" !!
    h = h0
    # da ugotovim na koliko intervalov je potrebno razdeliti območje, da dosežem zadnjo iskano ničlo, če računam s korakom h0
    X1, Y1, N  = magnus_method_until_enough_zeros(x0, y0, h, num_zeros) 

    # izračunam vrednosti s pol manjšim korakom na istem intervalu
    h = h/2
    N = 2*N
    X2, Y2  = magnus_method(x0, y0, N, h)

    if method == "last value"
        x1 = X1[end] # za preverjanje
        y1 = Y1[1][end]
        
        x2 = X2[end] # za preverjanje
        y2 = Y2[:, end][1]

        #println("dolzine: ", length(X1), " ", length(X2)) # preverim ali je dolžina X2 enaka 2*length(X1) - 1 
        #println("h: ", h)
        #println("last x: ", x1, " ", x2) # ali sta zadnji vrednosti x do katerih računam enaki
        #println("abs y: ", abs(y1 - y2))
        #sleep(5)

        while abs(y1 - y2) >= epsilon
            y1 = y2
            x1 = x2 # za preverjanje
            X1 = copy(X2) # za preverjanje

            # izračunam vrednosti s pol manjšim korakom na istem intervalu
            h = h/2
            N = 2*N
            X2, Y2  = magnus_method(x0, y0, N, h)

            x2 = X2[end]
            y2 = Y2[:, end][1]

            #println()
            #println("dolzine: ", length(X1), " ", length(X2)) # preverim ali je dolžina X2 enaka 2*length(X1) - 1 
            #println("h: ", h)
            #println("last x: ", x1, " ", x2) # ali sta zadnji vrednosti x do katerih računam enaki
            #println("abs y: ", abs(y1 - y2))
            #sleep(5)
        end

    elseif method == "all values" # NE DELA OK
        x1 = X1[end] # za preverjanje
        y1 = Y1[1]
        
        x2 = X2[end] # za preverjanje
        y2 = Y2[1, :]

        #println("dolzine: ", length(X1), " ", length(X2)) # preverim ali je dolžina X2 enaka 2*length(X1) - 1 
        #println("h: ", h)
        #println("last x: ", x1, " ", x2) # ali sta zadnji vrednosti x do katerih računam enaki
        
        #println("dolzine y: ", length(y1), " ", length(y2)) # preverim ali je dolžina y2 enaka 2*length(y1) - 1

        # zaokrožim na natančnost na katero računam (privzeto 10 decimalnih mest)
        y1 = [round(y1[i], sigdigits = 10) for i = 1:length(y1)]
        y2_processed = [round(y2[i], sigdigits = 10) for i = 1:length(y2) if i%2!=0] # odstranim vse vmesne izračune, ki jih nimam v y1
        #println("dolzine y: ", length(y1), " ", length(y2_processed)) # na tem mestu morata biti dolžini enaki
        #println("y1: ", y1[1:6])
        #println("y2: ", y2[1:11])
        #println("y2_processed: ", y2_processed[1:6])

        # preizkus
        #a = [sqrt(2), pi]
        #b = [sqrt(2) + 10^(-14), pi + 10^(-14)]
        #a == b #vrne false
        #aa = [round(i, sigdigits = 10) for i in a]
        #bb = [round(i, sigdigits = 10) for i in b]
        #aa == bb #vrne true


        while y1 != y2_processed
            y1 = copy(y2)
            x1 = x2 # za preverjanje
            X1 = copy(X2) # za preverjanje

            # izračunam vrednosti s pol manjšim korakom na istem intervalu
            h = h/2
            N = 2*N
            X2, Y2  = magnus_method(x0, y0, N, h)

            x2 = X2[end] # za preverjanje
            y2 = Y2[1, :]
            
            #println()
            #println("dolzine: ", length(X1), " ", length(X2)) # preverim ali je dolžina X2 enaka 2*length(X1) - 1
            #println("h: ", h)
            #println("last x: ", x1, " ", x2) # ali sta zadnji vrednosti x do katerih računam enaki

            #println("dolzine y: ", length(y1), " ", length(y2)) # preverim ali je dolžina y2 enaka 2*length(y1) - 1

            # zaokrožim na natančnost na katero računam (privzeto 10 decimalnih mest)
            y2_processed = [round(y2[i], sigdigits = 10) for i = 1:length(y2) if i%2!=0] # odstranim vse vmesne izračune, ki jih nimam v y1
            #println("dolzine y: ", length(y1), " ", length(y2_processed)) # na tem mestu morata biti dolžini enaki
            #println("y1: ", y1[1:6])
            #println("y2: ", y2[1:11])
            #println("y2_processed: ", y2_processed[1:6])
            #sleep(10)
        end
    end
    return h, N

end

"""
    find_n_zeros(x0, y0, num_zeros; h0 = -1/10, epsilon = 10^(-10), method = "bisection", h_forced = 0.0)

Najde x in y koordinate prvh `num_zeros` ničel Airijeve funkcije. 
Najprej na območju od x = `x0` do `num_zeros` z vnaprej določenim korakom `h_forced` (v primeru, ko `h_forced` < 0.0), oziroma s korakom, ki 
ga izračuna s pomočjo funkcije `find_h(x0, y0, h0, num_zeros)`, najde dobre približke za izračun ničel.
Z najdenimi približki nato z metodo bisekcije (argument `method` = "bisection") ali newtonove metode (argument `method` = "newton")  najde vrednosti 
ničel na natančnost `epsilon`. 
Funkcija vrne tudi vrednost koraka, ki ga je uporabila za dovolj natančen izračun funkcijskih vrednosti.
`y0` je seznam, katerega prvi element predstavlja vrednost Airijeve funkcije v `x0`, drugi pa predstavlja vrednost odvoda v `x0`.
"""
function find_n_zeros(x0, y0, num_zeros; h0 = -1/10, epsilon = 10^(-10), method = "bisection", h_forced = 0.0) #method = "newton"
    if h_forced < 0
        h = h_forced
    else
        h, N = find_h(x0, y0, h0, num_zeros)
    end

    println("uporabljen h: ", h)
    X_zeros_aprox, Y_zeros_aprox = find_aprox_n_zeros(x0, y0, h, num_zeros) 

    x_zeros = zeros(1, num_zeros)
    y_zeros = zeros(1, num_zeros)
    
    for zero_idx in 1:length(X_zeros_aprox)
        #println("zero idx: ", zero_idx)
        if method == "newton"
            x = X_zeros_aprox[zero_idx][1] # x koordinata točke pred sekanjem x osi (lahko bi vzela tudi drugo točko)
            y = Y_zeros_aprox[zero_idx][1][1] # y koordinata točke pred sekanjem x osi
            dy = Y_zeros_aprox[zero_idx][1][2] # naklon funkcije v točki pred sekanjem x osi
            #println(x, " ", y, " ", dy)

            while abs(y) >= epsilon
                x_new = x - y/dy
                h_newton = x_new - x
                X, Y  = magnus_method(x, [y, dy], 1, h_newton)
                x = X[end]
                y = Y[:, end][1]
                dy = Y[:, end][2]
                #println(x, " ", y, " ", dy)
            end

        elseif method == "bisection"
            x1 = X_zeros_aprox[zero_idx][1] # x koordinata prve točke pred sekanjem x osi
            y1 = Y_zeros_aprox[zero_idx][1][1] # y koordinata točke pred sekanjem x osi
            dy1 = Y_zeros_aprox[zero_idx][1][2] # naklon funkcije v točki pred sekanjem x osi

            x2 = X_zeros_aprox[zero_idx][2] # x koordinata točke po sekanju x osi
            y2 = Y_zeros_aprox[zero_idx][2][1] # y koordinata točke po sekanju x osi
            dy2 = Y_zeros_aprox[zero_idx][1][2] # naklon funkcije v točki po sekanju x osi
            
            x_new = (x1 + x2)/2
            h_bisection = x_new - x1 # vrednost funkcije v x_new dobim s pomočjo vrednosti v x1 (lahko bi z x2)
            X, Y  = magnus_method(x1, [y1, dy1], 1, h_bisection)
            x = X[end]
            y = Y[:, end][1]
            dy = Y[:, end][2]

            while abs(y) >= epsilon && abs(x1 - x2) >= epsilon
                if y1*y < 0
                    y2 = y
                    x2 = x
                    dy2 = dy
                else
                    y1 = y
                    x1 = x
                    dy1 = dy
                end
                x_new = (x1 + x2)/2
                h_bisection = x_new - x1
                X, Y  = magnus_method(x1, [y1, dy1], 1, h_bisection)
                x = X[end]
                y = Y[:, end][1]
                dy = Y[:, end][2]
            end
        end

        x_zeros[zero_idx] = x
        y_zeros[zero_idx] = y
    end
    return x_zeros, y_zeros, h
end




h0 = -1/10
x0 = 0
y0 = [1/(3^(2/3) * gamma(2/3)), -1/(3^(1/3) * gamma(1/3))]
num_zeros = 100

#x_zeros, y_zeros, h = find_n_zeros(x0, y0, num_zeros)