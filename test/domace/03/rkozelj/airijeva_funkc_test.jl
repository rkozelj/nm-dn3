# naj asistent ignorira spodnji dve vrstici
#cd("D:\\git\\Num-mat\\DN\\dn3\\nm-dn3\\test\\domace\\03\\rkozelj")
#include("airijeva_funkc_test.jl")

include("../../../../src/domaca03/rkozelj/airijeva_funkc.jl")

using Test

@testset "ničle airijeve funkcije" begin
    epsilon = 10^(-10)
    x0 = 0
    y0 = [1/(3^(2/3) * gamma(2/3)), -1/(3^(1/3) * gamma(1/3))]


    num_zeros = 5

    x_zeros, y_zeros = find_n_zeros(x0, y0, num_zeros; h0 = -1/10, epsilon = epsilon, method = "bisection")

    for zero_idx = 1: num_zeros
        @test abs(y_zeros[zero_idx] - 0) < epsilon # natančnost metode za iskanje ničel na 10 decimalk
        @test abs(y_zeros[zero_idx] - airyai(x_zeros[zero_idx])) < epsilon # preverjanje ustreznosti izračunanega koraka
    end

    x_zeros, y_zeros = find_n_zeros(x0, y0, num_zeros; h0 = -1/10, epsilon = epsilon, method = "newton")

    for zero_idx = 1: num_zeros
        @test abs(y_zeros[zero_idx] - 0) < epsilon 
        @test abs(y_zeros[zero_idx] - airyai(x_zeros[zero_idx])) < epsilon
    end



    num_zeros = 100

    x_zeros, y_zeros = find_n_zeros(x0, y0, num_zeros; h0 = -1/10, epsilon = epsilon, method = "bisection")

    for zero_idx = 1: num_zeros
        @test abs(y_zeros[zero_idx] - 0) < epsilon # natančnost metode za iskanje ničel na 10 decimalk
        @test abs(y_zeros[zero_idx] - airyai(x_zeros[zero_idx])) < epsilon # preverjanje ustreznosti izračunanega koraka
    end

    x_zeros, y_zeros = find_n_zeros(x0, y0, num_zeros; h0 = -1/10, epsilon = epsilon, method = "newton")

    for zero_idx = 1: num_zeros
        @test abs(y_zeros[zero_idx] - 0) < epsilon 
        @test abs(y_zeros[zero_idx] - airyai(x_zeros[zero_idx])) < epsilon
    end


    num_zeros = 288

    x_zeros, y_zeros = find_n_zeros(x0, y0, num_zeros; h0 = -1/10, epsilon = epsilon, method = "bisection")

    for zero_idx = 1: num_zeros
        @test abs(y_zeros[zero_idx] - 0) < epsilon # natančnost metode za isaknje ničel na 10 decimalk
        @test abs(y_zeros[zero_idx] - airyai(x_zeros[zero_idx])) < epsilon # preverjanje ustreznosti izračunanega koraka
    end

    x_zeros, y_zeros = find_n_zeros(x0, y0, num_zeros; h0 = -1/10, epsilon = epsilon, method = "newton")

    for zero_idx = 1: num_zeros
        @test abs(y_zeros[zero_idx] - 0) < epsilon 
        @test abs(y_zeros[zero_idx] - airyai(x_zeros[zero_idx])) < epsilon
    end
end
